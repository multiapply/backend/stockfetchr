const expect = require('chai').expect;
const sinon = require('sinon')
//const sandbox = require('sinon').createSandbox();
const fs = require('fs');
const Stockfetchr = require('../src/stockfetchr');

describe('Stockfetchr tests', () => {
  var stockfetchr;
  let sandbox;
  beforeEach(() => {
    sandbox = sinon.createSandbox();
    stockfetchr = new Stockfetchr();
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should pass this canary test', () => {
    expect(true).to.be.true;
  });

  describe('Read file', () => {
    it('should invoke error handler when reading an invalid file', function(done){
      // Assert (callback)
      var onError = function(err){
        expect(err).to.be.eql('Error reading file: InvalidFile');
        done()
      }

      // Arrange
      sandbox.stub(fs, 'readFile').callsFake((filename, callback) => {
        callback(new Error('failed'));
      })

      // Act
      stockfetchr.readTickersFile('InvalidFile', onError);
    });

    it('should invoke processTicker when reading a valid file', (done) => {
      // Arrange
      var rawData = 'GOOG\nAAPL\nORCL\ņMSFT';
      var parsedData = ['GOOG', 'AAPL', 'ORCL', 'MSFT'];

      // Arrange (stubs)
      sandbox.stub(stockfetchr, 'parseTickers')
              .withArgs(rawData).returns(parsedData);
      // Assert
      sandbox.stub(stockfetchr, 'processTickers').callsFake((data) => {
        expect(data).to.be.eql(parsedData);
        done();
      });

      sandbox.stub(fs, 'readFile').callsFake((filename, callback) => {
        callback(null, rawData);
      });

      stockfetchr.readTickersFile('tickers.txt');
    });

    it('should return error if given file is empty', (done) => {
      // Assert
      var onError = function(err){
        expect(err).to.be.eql('File tickers.txt has invalid content');
        done();
      };

      // Arrange
      sandbox.stub(stockfetchr, 'parseTickers')
              .withArgs('').returns([]);

      sandbox.stub(fs, 'readFile').callsFake((filename, callback) => {
        callback(null, '');
      });

      // Act
      stockfetchr.readTickersFile('tickers.txt', onError);
    });
  });
  describe('Parse Tickers', () => {
    it('should return tickers', () => {
      expect(stockfetchr.parseTickers("A\nB\nC")).to.be.eql(['A', 'B', 'C'])
    });

    it('should return empty array for empty content', () => {
      expect(stockfetchr.parseTickers("")).to.be.eql([]);
    });

    it('should return empty array for white-space', () => {
      expect(stockfetchr.parseTickers(" ")).to.be.eql([]);
    });

    it('should ignore unexpected format in content', () => {
      var rawData = "AAPL \t \nBla h\nGOOG\n\n    ";
      expect(stockfetchr.parseTickers(rawData)).to.be.eql(['GOOG']);
    });
  });

  describe('Process Tickers', () => {
    it('should call getPrice for each ticker symbol', () => {
      // Arrange (expects)
      var stockfetchrMock = sandbox.mock(stockfetchr);
      stockfetchrMock.expects('getPrice').withArgs('A');
      stockfetchrMock.expects('getPrice').withArgs('B');
      stockfetchrMock.expects('getPrice').withArgs('C');

      // Act
      stockfetchr.processTickers(['A', 'B', 'C']);

      // Assert
      stockfetchrMock.verify();
    });
    
    it('should save tickers count', () => {
      // Arrange
      sandbox.stub(stockfetchr, 'getPrice');
      // Act
      stockfetchr.processTickers(['A', 'B', 'C']);
      // Assert
      expect(stockfetchr.tickersCount).to.be.eql(3);
    });
  });

  describe('Get Price', () => {
    it('should call get on http with valid URL', (done) => {
      var httpStub = sandbox.stub(stockfetchr.http, 'get').callsFake((url) => {
        expect(url)
          .to.be.eql('http://ichart.finance.yahoo.com/table.csv?s=GOOG');
        done();
        return {on: function() {} };
      });

      stockfetchr.getPrice('GOOG');
    });

    it('should register a response handler for get request', (done) => {
      var aHandler = function() {};

      // Arrange (define interface) + Force Pass
      sandbox.stub(stockfetchr.processResponse, 'bind')
            .withArgs(stockfetchr, 'GOOG')
            .returns(aHandler);
      
      var httpStub = sandbox.stub(stockfetchr.http, 'get').callsFake((url,handler) => {
        expect(handler).to.be.eql(aHandler);
        done();
        return {on: function() {} };
      });

      stockfetchr.getPrice('GOOG');
    });

    it('should register an error handler for failure of reaching host', (done) => {
      var errorHandler = function() {};

      // Arrange (define interface) + Force Error
      sandbox.stub(stockfetchr.processHttpError, 'bind')
            .withArgs(stockfetchr, 'INVALID')
            .returns(errorHandler);

      // Assert
      var onStub = function(event, handler){
        expect(event).to.be.eql('error');
        expect(handler).to.be.eql(errorHandler);
        done();
      }

      // Arrange
      sandbox.stub(stockfetchr.http,'get').returns({on: onStub});

      // Act
      stockfetchr.getPrice('INVALID');
    });
  });

  describe('Process Response', () => {
    it('should call parsePrice with valid data', () => {
      var dataFunction;
      var endFunction;

      // Arrange (simulate http response session actions)
      var response = {
        statusCode: 200,
        on: function(event, handler) {
          if (event === 'data') dataFunction = handler;
          if (event === 'end') endFunction = handler;
        }
      };

      // Assert
      var parsePriceMock = sandbox.mock(stockfetchr)
            .expects('parsePrice')
            .withArgs('GOOG', 'some data');

      // Act (request)
      stockfetchr.processResponse('GOOG', response);
      // Act (response)
      dataFunction('some ');
      dataFunction('data');
      endFunction();

      // Assert
      parsePriceMock.verify();
    });

    it('should call processError if response failed', () => {
      // Arrange (http response)
      var response = { statusCode: 404 };

      // Assert
      var processErrorMock = sandbox.mock(stockfetchr)
              .expects('processError')
              .withArgs('GOOG', 404);

      // Act (request)
      stockfetchr.processResponse('GOOG', response);

      // Assert
      processErrorMock.verify();
    });
    it('should call processError only if response failed', () => {
      var response = {
        statusCode: 200,
        on: function() {}
      };
      var processErrorMock = sandbox.mock(stockfetchr)
                                    .expects('processError')
                                    .never();
      stockfetchr.processResponse('GOOG', response);
      processErrorMock.verify();
    });
    
  });

  describe('Process Http Error', () => { 
    it('should call processError with error details', () => {
      var processErrorMock = sandbox.mock(stockfetchr)
                                    .expects('processError')
                                    .withArgs('GOOG', '...error code...');
      var error = { code: '...error code...'};
      stockfetchr.processHttpError('GOOG', error);
      processErrorMock.verify();

    });
  });

  describe('Parse Price', () => { 
    var data = "Date,Open,High,Low,Close,Volume,Adj Close\n\
              2015-09-11,619.75,625.780029,617.419983,625.77002,1360900,625.77002\n\
              2015-09-10,613.099976,624.159973,611.429993,621.349976,1900500,621.349976";

    it('should update prices', () => {
      stockfetchr.parsePrice('GOOG', data);
      expect(stockfetchr.prices.GOOG).to.be.eql('625.77002')
    });

    it('should call printReport', () => {
      var printReportMock = sandbox.mock(stockfetchr).expects('printReport');

      stockfetchr.parsePrice('GOOG', data);
      printReportMock.verify();
    });
  });

  describe('Process Error', () => { 
    it('should update errors', () => {
      stockfetchr.processError('GOOG', '...oops...');
      expect(stockfetchr.errors.GOOG).to.be.eql('...oops...');
    });

    it('should call printReport', () => {
      var printReportMock = sandbox.mock(stockfetchr).expects('printReport');

      stockfetchr.processError('GOOG', '...oops...');
      printReportMock.verify();
    });
  });

  describe('Print Report', () => {
    it('should send price & errors once all responses arrive', () => {
      stockfetchr.prices = { 'GOOG': 12.34 };
      stockfetchr.errors = { 'AAPL' : 'error'};
      stockfetchr.tickersCount = 2;

      var callbackMock = sandbox.mock(stockfetchr)
            .expects('reportCallback')
            .withArgs([['GOOG', 12.34]], [['AAPL', 'error']]);

      stockfetchr.printReport();
      callbackMock.verify();
    });

    it('should not send before all responses arrive', () => {
      stockfetchr.prices = { 'GOOG': 12.34};
      stockfetchr.errors = { 'AAPL' : 'error'};
      stockfetchr.tickersCount = 3;

      var callbackMock = sandbox.mock(stockfetchr)
            .expects('reportCallback')
            .never();

      stockfetchr.printReport();
      callbackMock.verify()
    });

    it('should call formatAndSortTickers once for prices, once for errors', () => {
      stockfetchr.prices = { 'GOOG': 12.34 };
      stockfetchr.errors = { 'AAPL' : 'error'};
      stockfetchr.tickersCount = 2;

      var mock = sandbox.mock(stockfetchr);
      mock.expects('formatAndSortTickers').withArgs(stockfetchr.prices);
      mock.expects('formatAndSortTickers').withArgs(stockfetchr.errors);
      stockfetchr.printReport();
      mock.verify();
    });
  });

  describe('Format and Sort Tickers', () => { 
    it('should sort the data based on the symbols', () => {
      var tickersToSort = {
        'GOOG': 1.2,
        'AAPL': 2.1
      };
      var result = stockfetchr.formatAndSortTickers(tickersToSort);
      expect(result).to.be.eql([['AAPL', 2.1],['GOOG', 1.2]])
    });
   }) 
});