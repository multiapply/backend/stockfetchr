var fs = require('fs');
var http = require('http');

var Stockfetchr = function (){
  this.readTickersFile = (filename, onError) => {

    var processResponse = (err,data) => {
      if(err)
        onError('Error reading file: ' + filename);
      else {
        var tickers = this.parseTickers(data.toString());
        if (tickers.length === 0)
          onError('File ' + filename + ' has invalid content');
        else
          this.processTickers(tickers);
      }
    };

    fs.readFile(filename, processResponse);
  };

  this.parseTickers = function(content){
    var isInRightFormat = function(str){
      return str.length !== 0 && str.indexOf(' ') < 0;
    }
    return content.split('\n').filter(isInRightFormat);
  };

  this.processTickers = function(tickers){
    this.tickersCount = tickers.length;
    tickers.forEach(ticker => this.getPrice(ticker));
  };

  this.tickersCount = 0;
  
  this.http = http;
  this.getPrice = function(symbol){
    var url = `http://ichart.finance.yahoo.com/table.csv?s=${symbol}`;
    this.http.get(url, this.processResponse.bind(this,symbol))
              .on('error', this.processHttpError.bind(this,symbol));
  };

  this.processResponse = function(symbol,response) {
    var self = this;
    
    if(response.statusCode === 200) {
      var data = '';
      response.on('data', function(chunk) { data += chunk; });
      response.on('end', function() {self.parsePrice(symbol, data);});
    } else {
      self.processError(symbol, response.statusCode);
    }
  };
  this.processHttpError = function(ticker, error) {
    this.processError(ticker, error.code);
  };

  this.prices = {};

  this.parsePrice = function(ticker, data) {
    var price = data.split('\n')[1].split(',').pop();
    this.prices[ticker] = price;
    this.printReport();
  };
  this.errors = {};
  this.processError = function(ticker, error) {
    this.errors[ticker] = error;
    this.printReport();
  };

  this.printReport = function() {
    totalPrices = Object.keys(this.prices).length;
    totalErrors = Object.keys(this.errors).length;

    if(this.tickersCount === totalPrices + totalErrors)
      this.reportCallback(this.formatAndSortTickers(this.prices),this.formatAndSortTickers(this.errors));
  };

  this.formatAndSortTickers = function(tickers) {
    var toArray = function(key) { return [key, tickers[key]]};
    return Object.keys(tickers).sort().map(toArray);
  };

  this.reportCallback = function () {};
  
};

module.exports = Stockfetchr;